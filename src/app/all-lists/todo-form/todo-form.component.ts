import { Component, OnInit} from '@angular/core';
import { ITodo } from 'src/app/interfaces/todo.interface';
import { TodoService } from 'src/app/todo.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-todo-form',
  templateUrl: './todo-form.component.html',
  styleUrls: ['./todo-form.component.css']
})
export class TodoFormComponent implements OnInit{

  myTodo:ITodo;
  addDate:number = Date.now();

  constructor(public service:TodoService, 
    private firestore:AngularFirestore,
    private toastr: ToastrService) {

      setInterval(() => {
        this.addDate =  Date.now();
      }, 1);

    }

  ngOnInit() {
    this.resetForm();
  }

  dateFunction()
  {
    this.addDate = Date.now();
  }

  resetForm(form?: NgForm)
  {

    if(form != null)
    form.resetForm();
    this.service.todoForm = {
      id: null,
      text: '',
      description: '',
      done: null,
      date: this.addDate
    }
  }

  onAddNewTodo(form: NgForm)
  {
    let data = form.value;
    this.firestore.collection('todos').add(data);
    this.resetForm(form);
    this.toastr.success('Tâche ajoutée');

  }



}
