import { Component, OnInit} from '@angular/core';
import { ITodo } from '../interfaces/todo.interface';
import { TodoService } from '../todo.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { ToastrService } from 'ngx-toastr';
import { trigger, transition, state, style, animate } from '@angular/animations';


@Component({
  selector: 'app-all-lists',
  templateUrl: './all-lists.component.html',
  providers: [ TodoService ],
  styleUrls: ['./all-lists.component.css'],
  animations:[
    trigger('fade', [
      transition('void => *', [
        style({ opacity:0 }),
        animate(2000)
      ])
    ])
  ]

})
export class AllListsComponent implements OnInit {

  editTodo:number;
  editTodoBool:boolean = false;
  listName:string = 'Ma Todo List';
  todoList:Array<ITodo> = [];
  nbTask:number = 0;
  toDestroy:Array<any>;
  displayedColumns:Array<ITodo> = [];
  idToDestroy:Array<string> = [];
  confirmDestroyAll:number = 0;
  anotherConfirmDestroyAll:boolean = false;
  destroy:boolean = false;
  sortDateBool:boolean = null;
  sortTextBool:boolean;
  sortDescBool:boolean;
  sortDoneBool:boolean;

  constructor(private service:TodoService, 
    private firestore:AngularFirestore,
    private toastr: ToastrService){

  }

  ngOnInit()
  {

    this.service.getTodoList().subscribe(actionArray => {
      this.todoList = actionArray.map(item => {
        return {
          id: item.payload.doc.id,
          ...item.payload.doc.data()
        } as ITodo;
      })
      this.nbTask = this.todoList.length;
    });    

  }

  sortByDate()
  {

    this.sortDateBool = !this.sortDateBool;
    this.sortTextBool = null;
    this.sortDescBool = null;
    this.sortDoneBool = null;

    if(this.sortDateBool)
    {
      this.service.getTodoListByDate().subscribe(actionArray => {
        this.todoList = actionArray.map(item => {
          return {
            id: item.payload.doc.id,
            ...item.payload.doc.data()
          } as ITodo;
        })
      }); 
    }
    else
    {
      this.service.getTodoList().subscribe(actionArray => {
        this.todoList = actionArray.map(item => {
          return {
            id: item.payload.doc.id,
            ...item.payload.doc.data()
          } as ITodo;
        })
      }); 
    }

  }

  
  sortByText()
  {

    this.sortTextBool = !this.sortTextBool;
    this.sortDateBool = null;
    this.sortDescBool = null;
    this.sortDoneBool = null;

    if(this.sortTextBool)
    {
      this.service.getTodoListByTextAsc().subscribe(actionArray => {
        this.todoList = actionArray.map(item => {
          return {
            id: item.payload.doc.id,
            ...item.payload.doc.data()
          } as ITodo;
        })
      }); 
    }
    else
    {
      this.service.getTodoListByTextDesc().subscribe(actionArray => {
        this.todoList = actionArray.map(item => {
          return {
            id: item.payload.doc.id,
            ...item.payload.doc.data()
          } as ITodo;
        })
      }); 
    }

  }

  sortByDesc()
  {

    this.sortDescBool = !this.sortDescBool;
    this.sortTextBool = null;
    this.sortDateBool = null;
    this.sortDoneBool = null;

    if(this.sortDescBool)
    {
      this.service.getTodoListByDescriptionAsc().subscribe(actionArray => {
        this.todoList = actionArray.map(item => {
          return {
            id: item.payload.doc.id,
            ...item.payload.doc.data()
          } as ITodo;
        })
      }); 
    }
    else
    {
      this.service.getTodoListByDescriptionDesc().subscribe(actionArray => {
        this.todoList = actionArray.map(item => {
          return {
            id: item.payload.doc.id,
            ...item.payload.doc.data()
          } as ITodo;
        })
      }); 
    }

  }

  sortByDone()
  {

    this.sortDoneBool = !this.sortDoneBool;
    this.sortTextBool = null;
    this.sortDescBool = null;
    this.sortDateBool = null;

    if(this.sortDoneBool)
    {
      this.service.getTodoListByDoneAsc().subscribe(actionArray => {
        this.todoList = actionArray.map(item => {
          return {
            id: item.payload.doc.id,
            ...item.payload.doc.data()
          } as ITodo;
        })
      }); 
    }
    else
    {
      this.service.getTodoListByDoneDesc().subscribe(actionArray => {
        this.todoList = actionArray.map(item => {
          return {
            id: item.payload.doc.id,
            ...item.payload.doc.data()
          } as ITodo;
        })
      }); 
    }

  }

  onChangeStatus(todo)
  {

    if(todo.done)
    {
      todo.done = false;
    }
    else
    {
      todo.done = true;
    }

    let todoValidEdit:ITodo = {
      id: todo.id,
      text: todo.text,
      description: todo.description,
      done: todo.done,
      date: todo.date
    }

    this.firestore.doc('todos/'+todo.id).update(todoValidEdit);
    this.toastr.success('Statut mis à jour');
  }

  onDelTodo(todo)
  {

    if(confirm('Etes-vous sure de vouloir supprimer la tâche'))
    {
      this.firestore.doc('todos/'+todo.id).delete();
      this.toastr.warning('Tâche supprimée');
    }
  }

  onDelAll()
  {

    if(this.confirmDestroyAll == 0 && this.anotherConfirmDestroyAll == false)
    {
      
      if(confirm('Vous êtes sur le point de supprimer toutes les tâches. Confirmez puis cliquez une nouvelle fois sur supprimer.'))
      {
        this.confirmDestroyAll++;
      }

    }


    if(this.confirmDestroyAll == 1 || this.anotherConfirmDestroyAll == true)
    {

      if(this.anotherConfirmDestroyAll)
      {
        if(!confirm('Toutes les tâches seront supprimées si vous validez'))
        {
          return
        }

        this.destroy = true;

        
      }

      this.service.getTodoList().subscribe(actionArray => {
        this.toDestroy = actionArray.map(item => {
          return {
            id: item.payload.doc.id,
            ...item.payload.doc.data()
          } as ITodo;
        })
        
        for(let i = 0; i < this.toDestroy.length; i++)
        {
          this.idToDestroy.push(this.toDestroy[i].id);
        }

      });

        this.service.deleteAllDocuments(this.idToDestroy);


        if(this.destroy == true && this.anotherConfirmDestroyAll == true)
        {
          this.toastr.error('Toutes les tâches ont été supprimés');
          this.confirmDestroyAll = 0;
          this.anotherConfirmDestroyAll = false;
        }

        this.anotherConfirmDestroyAll = true;

    }  


  }

  onEditName(todo)
  {
    this.editTodoBool = !this.editTodoBool;
    this.editTodo = todo.id;

    if(!this.editTodoBool)
    {

      let todoValidEdit:ITodo = {
        id: todo.id,
        text: todo.text,
        description: todo.description,
        done: todo.done,
        date: todo.date
      }

      this.firestore.doc('todos/'+todo.id).update(todoValidEdit);
      this.toastr.success('Tâche modifiée');
      
    }


  }






  



}
