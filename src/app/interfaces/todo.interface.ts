export interface ITodo {
    id: string,
    text : string,
    description?: string,
    done: boolean,
    date:number
}