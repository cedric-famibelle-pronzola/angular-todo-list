# Création d'une TodoList avec Angular 7
## Créer une application Angular qui permet de créer, afﬁcher et modiﬁer une liste de tâche à faire (todolist). 

[![Gif Angular Todo List]( http://www.cedric-famibelle.fr/angular-todo-list/angular_to_do_list.gif)](https://youtu.be/IXbX3-AN64w)


[Disponible ici](http://www.cedric-famibelle.fr/samnipoufe) -> http://www.cedric-famibelle.fr/samnipoufe


### Prérequis pour l'utilisation du repo Git :
* [NodeJS](https://nodejs.org)
* [Npm](https://nodejs.org/en/docs/meta/topics/dependencies/)
* [Angular CLI](https://angular.io/cli)

## Après avoir cloner le repo, installer les modules nécessaires grâce à la commande :
`
npm install
` ou alors `npm i`

Cette commande doit être effectuée à la racine du projet Angular.

## Après l'installation de tous les modules, exécuter la commande :
`ng serve --open` ou alors `ng serve -o`

La page de l'application angular s'ouvrira automatiquement.



